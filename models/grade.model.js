const { Schema, model, Types } = require('mongoose');

const gradeSchema = new Schema({
    id: {
        type: String,
        unique: true, 
        required: true
    },
    lastName: {
        type: String,
        required: true,
    },
    group: {
        type: String,
        required: true,
    },
    subject: {
        type: String,
        required: true,
    },
    ticketNumber: {
        type: Number,
        required: true,
    },
    grade: {
        type: Number,
        required: true,
    },
    teacher: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

const grade = model('grade', gradeSchema);

module.exports = grade;
