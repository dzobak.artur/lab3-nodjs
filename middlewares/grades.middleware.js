const createError = require('http-errors');
const ObjectId = require('mongoose').Types.ObjectId;
const gradeService = require('../services/grades.service');

async function gradeByIdValidation(req, res, next) {
    try {
        const { gradeId } = req.params;

        if (!ObjectId.isValid(gradeId)) {
            throw createError.BadRequest("Grade id is not valid");
        }

        const grade = await gradeService.getGrade(gradeId);

        if (!grade) {
            throw createError.NotFound("Grade with such id not found");
        }

       // if (grade.grade <= 80) {
         //   throw createError.BadRequest("Grade is not greater than 80");
        //}
        //const grades = await gradeService.fetchGrades();

        //if (grades.grade.grade <= 60){

        //}

        next();
    } catch(err) {
        next(err);
    }
};

async function gradesGreaterThanSixty(req, res, next) {
    try {
        const grades = await gradeService.fetchGrades({}); 

    
        const filteredGrades = grades.items.filter(grade => grade.grade > 10);


        const selectedGrades = filteredGrades.map(grade => ({
            lastName: grade.lastName,
            group: grade.group,
            subject: grade.subject,
            ticketNumber: grade.ticketNumber,
            grade: grade.grade,
            teacher: grade.teacher
        }));

        res.status(200).json({
            status: 200,
            data: {
                items: selectedGrades,
                count: selectedGrades.length
            }
        });
    } catch(err) {
        next(err);
    }
}


module.exports = {
    gradeByIdValidation,
    gradesGreaterThanSixty,
    
};